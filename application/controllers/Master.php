<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_status_login();
        $this->load->model('User_model');
        $this->load->model('Penyakit_model');
        $this->load->model('Rule_model');
    }

    public function index()
    {

        $data['title'] = 'Master Penyakit';
        $data['user'] = $this->User_model->get_detail_users($this->session->userdata('user_id'));
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('master/penyakit', $data);
        $this->load->view('templates/footer');
    }

    function data_penyakit(){
        $data=$this->Penyakit_model->penyakit_list();
        echo json_encode($data);
    }
 
    function get_penyakit(){
        $id_penyakit=$this->input->get('id_penyakit');
        $data=$this->Penyakit_model->get_penyakit_by_id($id_penyakit);
        echo json_encode($data);
    }
 
    function save_penyakit(){
        $id_penyakit             =$this->input->post('id_penyakit');
        $nama_penyakit  =$this->input->post('nama_penyakit');
        $solusi         =$this->input->post('solusi');
        $data           =$this->Penyakit_model->save_penyakit($id_penyakit,$nama_penyakit,$solusi);
        echo json_encode($data);
    }
 
    function update_penyakit(){
        $id_penyakit             =$this->input->post('id_penyakit');
        $nama_penyakit  =$this->input->post('nama_penyakit');
        $solusi         =$this->input->post('solusi');
        $data           =$this->Penyakit_model->update_penyakit($id_penyakit,$nama_penyakit,$solusi);
        echo json_encode($data);
    }
 
    function delete_penyakit(){
        $id_penyakit         =$this->input->post('id_penyakit');
        $data       =$this->Penyakit_model->delete_penyakit($id_penyakit);
        echo json_encode($data);
    }

    public function gejala()
    {

        $data['title'] = 'Master Gejala Penyakit';
        $data['user'] = $this->User_model->get_detail_users($this->session->userdata('user_id'));
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('master/gejala', $data);
        $this->load->view('templates/footer');
    }


    function data_gejala(){
        $data=$this->Penyakit_model->gejala_list();
        echo json_encode($data);
    }
 
    function get_gejala(){
        $id_gejala=$this->input->get('id_gejala');
        $data=$this->Penyakit_model->get_gejala_by_id($id_gejala);
        echo json_encode($data);
    }
 
    function save_gejala(){
        $id_gejala             =$this->input->post('id_gejala');
        $nama_gejala  =$this->input->post('nama_gejala');
        $data           =$this->Penyakit_model->save_gejala($id_gejala,$nama_gejala);
        echo json_encode($data);
    }
 
    function update_gejala(){
        $id_gejala    =$this->input->post('id_gejala');
        $nama_gejala  =$this->input->post('nama_gejala');
        $data           =$this->Penyakit_model->update_gejala($id_gejala,$nama_gejala);
        echo json_encode($data);
    }
 
    function delete_gejala(){
        $id_gejala         =$this->input->post('id_gejala');
        $data       =$this->Penyakit_model->delete_gejala($id_gejala);
        echo json_encode($data);
    }

    public function rule()
    {

        $data['title'] = 'Rule Chaining';
        $data['user'] = $this->User_model->get_detail_users($this->session->userdata('user_id'));
        $data['rule'] = $this->Rule_model->rule_list();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('master/rule', $data);
        $this->load->view('templates/footer');
    }

    public function add_rule()
    {

        $data['title']      = 'Add Rule Chaining';
        $data['user']       = $this->User_model->get_detail_users($this->session->userdata('user_id'));
        $data['penyakit']   = $this->Penyakit_model->penyakit_list();
        $data['gejala']     = $this->Penyakit_model->gejala_list();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('master/add_rule', $data);
        $this->load->view('templates/footer');
    }

    public function add_rule_process()
    {

        $id_penyakit    = $this->input->post('id_penyakit');
        $gejala         = $this->input->post('gejala');
        try {
            // insert rule first
            $rule_id           = $this->Rule_model->save_rule($id_penyakit);
            $data              = $this->Rule_model->save_rule_detail($gejala, $rule_id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Rule saved succesfully !!</div>');
        } catch (Exception $e) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Rule failed to save !!</div>');
        }
        
        redirect('master/rule');
  
    }

    public function edit_rule($id)
    {
        if (empty($id) || $id == '')  {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Internal Server Error !!</div>');
            redirect('master/rule');
        }else{
            $data['title']      = 'Add Rule Chaining';
            $data['user']       = $this->User_model->get_detail_users($this->session->userdata('user_id'));
            $data['penyakit']   = $this->Penyakit_model->penyakit_list();
            // get detail rule
            $detail_chain = $this->Rule_model->get_rule_by_id($id);
            $data['id_penyakit']= $detail_chain['id_penyakit'];
            $data['gejala']     = $this->Rule_model->get_rule_chain_by_id($id);
            $data['rule_id']    = $id;

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('master/edit_rule', $data);
            $this->load->view('templates/footer');
        }

    }

    public function edit_rule_process()
    {

        $id_penyakit    = $this->input->post('id_penyakit');
        $id_rule    = $this->input->post('rule_id');
        $gejala         = $this->input->post('gejala');
        try {
            // insert rule first
            $this->Rule_model->update_rule($id_penyakit,$id_rule);
            $this->Rule_model->update_rule_detail($gejala, $id_rule);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Rule saved succesfully !!</div>');
        } catch (Exception $e) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Rule failed to save !!</div>');
        }
        
        redirect('master/rule');
  
    }


    public function delete_rule($id)
    {
        if (empty($id) || $id == '')  {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Internal Server Error !!</div>');
            redirect('master/rule');
        }else{
            $this->Rule_model->delete_rule($id);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Delete Success</div>');
            redirect('master/rule');
            
        }

    }
        
}