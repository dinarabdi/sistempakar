<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Analisa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        check_status_login();
        $this->load->model('User_model');
        $this->load->model('Penyakit_model');
        $this->load->model('Rule_model');
    }

    public function index()
    {

        $data['title'] = 'Analisa Penyakit';
        $data['user'] = $this->User_model->get_detail_users($this->session->userdata('user_id'));
        $gejala = $this->Penyakit_model->gejala_list();
        $gejala_new = [];
        foreach ($gejala as $value) {
            $gejala_new[] = ['id_gejala' => $value->id_gejala, 'nama_gejala' => $value->nama_gejala];
        }

        $gejala_value = $this->input->post('gejala_value');
        $gejala_key = $this->input->post('gejala_key');
        $id_gejala = $this->input->post('id_gejala');



        if (!empty($id_gejala)) {

            $saved_array = $this->session->get_userdata('gejala_saved');
            if (empty($saved_array['gejala_saved'])) {
                $saved_array['gejala_saved'] = [];
            }
            $post_array[$gejala_key] = ['id_gejala' => $id_gejala, 'gejala_value' => $gejala_value];

            $saved_array = array_merge($saved_array['gejala_saved'], $post_array);
            $this->session->set_userdata('gejala_saved', $saved_array);

            if (!empty($gejala_new[$gejala_key+1])) {
                $data['gejala'] = $gejala_new[$gejala_key+1];
                $data['gejala_key'] = $gejala_key+1;

                $this->load->view('templates/header', $data);
                $this->load->view('templates/sidebar', $data);
                $this->load->view('templates/topbar', $data);
                $this->load->view('analisa/index', $data);
                $this->load->view('templates/footer');

            }else{
                $this->perhitungan($saved_array);
            }

        }else{
            $saved_array = $this->session->unset_userdata('gejala_saved');
            $data['gejala'] = $gejala_new[0];
            $data['gejala_key'] = 0;


            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('analisa/index', $data);
            $this->load->view('templates/footer');

        }
        
    }


    public function perhitungan($saved_array)
    {
        // get aggreed gejala
        $id = [];
        foreach ($saved_array as $key => $value) {
            if ($value['gejala_value'] == 1) {
                $id[] = $value['id_gejala'];
            }
        }

        $id = array_unique($id);
        $data['title'] = 'Analisa Penyakit';
        $data['user'] = $this->User_model->get_detail_users($this->session->userdata('user_id'));
        // get analisa
        $data['hasil'] = $this->Rule_model->get_analisa($id);
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('analisa/hasil', $data);
        $this->load->view('templates/footer');

    }
        
}