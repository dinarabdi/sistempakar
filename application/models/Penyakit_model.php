<?php
class Penyakit_model extends CI_Model{
 
    function penyakit_list(){
        $hasil=$this->db->query("SELECT * FROM penyakit");
        return $hasil->result();
    }
 
    function save_penyakit($id_penyakit,$nama_penyakit,$solusi){
        $hasil=$this->db->query("INSERT INTO penyakit (id_penyakit,nama_penyakit,solusi)VALUES('$id_penyakit','$nama_penyakit','$solusi')");
        return $hasil;

    }


    function get_penyakit_by_id($id_penyakit){
        $sql = "SELECT * FROM penyakit
                WHERE id_penyakit = ?";
        $query = $this->db->query($sql, $id_penyakit);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
 
    function update_penyakit($id_penyakit,$nama_penyakit,$solusi){
        $hasil=$this->db->query("UPDATE penyakit SET nama_penyakit='$nama_penyakit',solusi='$solusi' WHERE id_penyakit='$id_penyakit'");
        return $hasil;
    }
 
    function delete_penyakit($id_penyakit){
        $hasil=$this->db->query("DELETE FROM penyakit WHERE id_penyakit='$id_penyakit'");
        return $hasil;
    }

    //Model Gejala
    function gejala_list(){
        $hasil=$this->db->query("SELECT * FROM gejala");
        return $hasil->result();
    }
 
    function save_gejala($id_gejala,$nama_gejala){
        $hasil=$this->db->query("INSERT INTO gejala (id_gejala,nama_gejala)VALUES('$id_gejala','$nama_gejala')");
        return $hasil;

    }

    function get_gejala_by_id($id_gejala){
        $sql = "SELECT * FROM gejala
                WHERE id_gejala = ?";
        $query = $this->db->query($sql, $id_gejala);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
 
    function update_gejala($id_gejala,$nama_gejala){
        $hasil=$this->db->query("UPDATE gejala SET nama_gejala='$nama_gejala' WHERE id_gejala='$id_gejala'");
        return $hasil;
    }
 
    function delete_gejala($id_gejala){
        $hasil=$this->db->query("DELETE FROM gejala WHERE id_gejala='$id_gejala'");
        return $hasil;
    }
     
}