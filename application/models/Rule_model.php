<?php
class Rule_model extends CI_Model{
 
    function rule_list(){
        $hasil=$this->db->query("SELECT rc.*, p.nama_penyakit FROM rule_chain rc
                                 LEFT JOIN penyakit p ON rc.id_penyakit = p.id_penyakit");
        return $hasil->result_array();
    }
 
    function save_rule($id_penyakit){
        $hasil=$this->db->query("INSERT INTO rule_chain (id_penyakit)VALUES('$id_penyakit')");
        $insert_id = $this->db->insert_id();
        return $insert_id;

    }

    function save_rule_detail($gejala, $rule_id){

        foreach ($gejala as $key => $value) {
            $hasil=$this->db->query("INSERT INTO rule_detail (id_rule,id_gejala,value)VALUES('$rule_id','$key','$value')");
        }
        
        return $hasil;

    }

    function get_rule_by_id($id){
        $sql = "SELECT * FROM rule_chain
                WHERE id = ?";
        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_rule_chain_by_id($id){
        $sql = "SELECT g.id_gejala, g.nama_gejala, IFNULL(rd.value, 0) as value FROM gejala g
                LEFT JOIN rule_detail rd ON g.id_gejala = rd.id_gejala AND rd.id_rule = ?";
        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    function get_analisa($id){
        $id = implode(',', $id);
        $sql = "select rc.*, p.nama_penyakit, p.solusi, GROUP_CONCAT(rd.id_gejala order by rd.id) as gejala from rule_chain rc
                inner join rule_detail rd on rc.id = rd.id_rule  and rd.value <> 0
                left join penyakit p on rc.id_penyakit = p.id_penyakit
                group by rc.id
                having gejala = ?";
        $query = $this->db->query($sql, $id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
           
            return $result;
        } else {
            return array();
        }
    }


    function update_rule($id_penyakit, $id_rule){
        $hasil=$this->db->query("UPDATE rule_chain SET id_penyakit='$id_penyakit' WHERE id='$id_rule'");
        return $hasil;
    }

    function update_rule_detail($gejala, $id_rule){
        // check rule detail
        foreach ($gejala as $key => $value) {
            $params = [$id_rule, $key];
            $sql = "SELECT * FROM rule_detail 
                WHERE id_rule = ? AND id_gejala = ?";
            $query = $this->db->query($sql, $params);
            if ($query->num_rows() > 0) {
                // do update
                $this->db->query("UPDATE rule_detail SET value='$value' WHERE id_rule='$id_rule' AND id_gejala='$key'");
            } else {
                // do insert
                $this->db->query("INSERT INTO rule_detail (id_rule,id_gejala,value)VALUES('$id_rule','$key','$value')");
            }
        }
    }

    function delete_rule($id){
        $hasil=$this->db->query("DELETE FROM rule_chain WHERE id='$id'");
        return $hasil;
    }
     
}