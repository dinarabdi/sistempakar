<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-10">
            <?php if (validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors(); ?>
                    </div>
                <?php endif; ?>

                <?= $this->session->flashdata('message'); ?>
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $error; ?>
                    </div>
            <?php } ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSubMenuModal">Add New Staff</a>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Username</th>
                        <th scope="col">Image</th>
                        <th scope="col">Status Active</th>
                        <th scope="col">Role</th>
                        <th scope="col">Date Created</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                        <?php $i = 1; ?>
                        <?php foreach ($staff as $s) : ?>
                            <tr>
                                <th scope="row"><?= $i; ?></th>
                                <td><?= $s['name']; ?></td>
                                <td><?= $s['username']; ?></td>
                                <td><img src="<?php echo base_url(); ?>assets/img/profile/<?= $s['image'] ?>" onerror="this.src='<?= base_url('assets/image/profile/default.png'); ?>';" alt="Missing Image" class="img-square img-responsive" width="100px" /></td>
                                <td>
                                    <?php if ($s['is_active'] == '1') { ?>
                                        <label class="label label-primary">Active</label>
                                    <?php } else { ?>
                                        <label class="label label-warning">Not ACtive</label>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if ($s['role_id'] == '1') { ?>
                                        <label class="label label-danger">Admin</label>
                                    <?php } else { ?>
                                        <label class="label label-primary">User</label>
                                    <?php } ?>
                                </td>
                                <td><?= $s['date_created']; ?></td>
                                <td>
                                    <a data-toggle="modal" data-target="#edit-data-<?php echo $s['id'] ?>" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#newEditModal">
                                        <li class=" fa fa-edit"> Edit</li>
                                    </a>
                                     <a onclick="deleteConfirm(' <?= base_url(); ?>staff/delete/<?= $s['id'] ?>')" href="#!" class="btn btn-danger btn-sm">
                                        <li class="fa fa-trash"> Delete</li>
                                    </a>
                                </td>

                                <!-- Modal edit-->
                                        <div class="modal fade" id="edit-data-<?php echo $s['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="newEditModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="newSubMenuModalLabel">Edit Staff</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="<?= base_url('staff/edit_process'); ?>" method="post" enctype="multipart/form-data">
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input name="id" type="hidden" value="<?= $s['id']; ?>">
                                                                <input type="text" class="form-control" id="username" name="username" placeholder="Username" readonly value="<?= $s['username']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?= $s['name']; ?>">
                                                            </div>
                                                             <div class="form-group">
                                                                  <div class="custom-file">
                                                                   
                                                                    <input type="file" class="form-control" name="image" id="image">
                                                                 </div>
                                                            </div>
                                                            <div class="form-group">
                                                               <select name="role_id" id="role_id" class="form-control">
                                                                    <option value="">Select Menu</option>
                                                                    <?php foreach ($role as $r) : ?>
                                                                        <option value="<?= $r['id']; ?>" 
                                                                            <?php if ($s['role_id'] == $r['id']) {echo "selected";} ?>
                                                                        ><?= $r['role']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <select name="is_active" id="is_active" class="form-control">
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Not Active</option>
                                                                </select>
                                                            </div>
                                                            <h5 style="color: red" class="">Kosongi password jika tidak ingin diganti !!</h5>
                                                            <div class="form-group">
                                                                <input type="password" class="form-control" id="password" name="password" placeholder="password">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Repeat Password">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="Submit" class="btn btn-primary">Update </button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                    
                                <?php $i++; ?>
                            </tr> <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

      <!-- Modal add-->
                                        <div class="modal fade" id="newSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="newSubMenuModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="newSubMenuModalLabel">Add New Staff</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="<?= base_url('staff/add_process'); ?>" method="post" enctype="multipart/form-data">
                                                        <div class="modal-body">

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                                            </div>
                                                             <div class="form-group">
                                                                  <div class="custom-file">
                                                                    <input type="file" class="form-control" name="image" id="image">
                                                                 </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <select name="role_id" id="role_id" class="form-control">
                                                                    <option value="">Select Menu</option>
                                                                    <?php foreach ($role as $r) : ?>
                                                                        <option value="<?= $r['id']; ?>"><?= $r['role']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <select name="is_active" id="is_active" class="form-control">
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Not Active</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" class="form-control" id="password" name="password" placeholder="password">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Repeat Password">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="Submit" class="btn btn-primary">Add </button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


<!-- Modal Delete -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            </div>
            <div class="modal-body">Are you sure you want to delete data?</div>
            <div class="modal-footer">
                <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script>
    function deleteConfirm(url) {
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
