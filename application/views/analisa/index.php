<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-12">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <h4 style="text-align: center;">Jawab Pertanyaan Berikut ini untuk menentukan analisa terhadap penyakit kucing anda</h4>
            <br>
            <br>
            <h6 style="text-align: center;">Apakah Kucing anda mengalami <?=$gejala['nama_gejala']?> ?</h6><br>
            <form method="post" class="form-horizontal" style="text-align: center;">
                <input type="hidden" name="id_gejala" value="<?=$gejala['id_gejala']?>">
                <input type="hidden" name="gejala_key" value="<?=$gejala_key?>">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gejala_value" id="inlineRadio1" value="1">
                  <label class="form-check-label" for="inlineRadio1">Ya</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="gejala_value" id="inlineRadio2" value="0">
                  <label class="form-check-label" for="inlineRadio2">Tidak</label>
                </div>
                <br>
                <br>
                <button type="submit" class="btn btn-success">Next</button>
            </form>
            
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.js'?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>


