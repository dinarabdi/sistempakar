<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-12">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <h4 style="text-align: center;">Kesimpulan</h4>
            <br>
            <br>
            <h6 style="text-align: center;">Kucing mengalami <?=$hasil[0]['nama_penyakit']?> ?</h6><br>
            Solusi : <?=$hasil[0]['solusi']?>
            
            
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.js'?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>


