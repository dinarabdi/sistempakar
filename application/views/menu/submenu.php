<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-10">
            <?php if (validation_errors()) : ?>
                <div class="alert alert-danger col-lg-6" role="alert">
                    <?= validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newSubMenuModal">Add New SubMenu</a>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Menu</th>
                        <th scope="col">Url</th>
                        <th scope="col">Icon</th>
                        <th scope="col">Active</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($subMenu as $sm) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $sm['title']; ?></td>
                            <td><?= $sm['menu']; ?></td>
                            <td><?= $sm['url']; ?></td>
                            <td><?= $sm['icon']; ?></td>
                           <td>
                                <?php if ($sm['is_active'] == '1') { ?>
                                    <label class="label label-primary">Active</label>
                                <?php } else { ?>
                                    <label class="label label-danger">Not Active</label>
                                <?php } ?>
                            </td>
                           <td>
                                <a data-toggle="modal" data-target="#edit-data-<?php echo $sm['id'] ?>" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#newEditModal">
                                    <li class=" fa fa-edit"> Edit</li>
                                </a>
                                <a onclick="deleteConfirm(' <?= base_url(); ?>menu/hapusSubMenu/<?= $sm['id'] ?>')" href="#!" class="btn btn-danger btn-sm">
                                    <li class="fa fa-trash"> Delete</li>
                                </a>
                            </td>

                             <!-- Modal Delete -->
                                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    </div>
                                                    <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
                                                    <div class="modal-footer">
                                                        <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
                                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Modal Edit-->
                                        <div class="modal fade" id="edit-data-<?php echo $sm['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="newEditModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="newEditModalLabel">Edit SubMenu</h5>
                                                    </div>
                                                    <form action="<?= base_url('menu/ubahSubMenu'); ?>" method="post">
                                                        <div class="modal-body">

                                                            <div class="form-group">
                                                                <input name="id" type="hidden" value="<?= $sm['id']; ?>">
                                                                <input type="text" class="form-control" id="title" name="title" placeholder="Submenu Title" value="<?= $sm['title']; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <select name="menu_id" id="menu_id" class="form-control">
                                                                    <?php foreach ($menu as $m) : ?>
                                                                        <option value="<?= $m['id']; ?>"
                                                                            <?php if ($sm['menu_id'] == $m['id']) {
                                                                                echo "selected";
                                                                                } ?>><?= $m['menu']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="url" name="url" placeholder="Submenu Url" value="<?= $sm['url']; ?>">
                                                            </div>
                                                            <div class=" form-group">
                                                                <input type="text" class="form-control" id="icon" name="icon" placeholder="Submenu Icon" value="<?= $sm['icon']; ?>">
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="checkbox">
                                                                    <label><input type="checkbox" value="1" name="is_active" id="is_active" checked>Active?</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="Submit" class="btn btn-primary">Update </button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- Modal add-->
                                        <div class="modal fade" id="newSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="newSubMenuModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="newSubMenuModalLabel">Add New SubMenu</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="<?= base_url('menu/submenu'); ?>" method="post">
                                                        <div class="modal-body">

                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="title" name="title" placeholder="SubMenu title">
                                                            </div>

                                                            <div class="form-group">
                                                                <select name="menu_id" id="menu_id" class="form-control">
                                                                    <option value="">Select Menu</option>
                                                                    <?php foreach ($menu as $m) : ?>
                                                                        <option value="<?= $m['id']; ?>"><?= $m['menu']; ?></option>
                                                                    <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="url" name="url" placeholder="SubMenu Url">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="icon" name="icon" placeholder="SubMenu icon">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-check">
                                                                    <input class="form-check-input" type="checkbox" value="1" id="is_active" name="is_active" checked>
                                                                    <label class="form-check-label" for="is_active">
                                                                        Active?
                                                                    </label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <button type="Submit" class="btn btn-primary">Add </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Modal -->
<script>
        function deleteConfirm(url) {
            $('#btn-delete').attr('href', url);
            $('#deleteModal').modal();
        }
    </script>