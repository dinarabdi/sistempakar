<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-6">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#newMenuModal">Add New Menu</a>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Menu</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($menu as $m) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $m['menu']; ?></td>
                            <td>
                                <a data-toggle="modal" data-target="#edit-data-<?php echo $m['id'] ?>" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#newEditModal">
                                    <li class="fa fa-edit"> Edit</li>
                                </a>
                                 <a onclick="deleteConfirm(' <?= base_url(); ?>menu/hapus/<?= $m['id'] ?>')" href="#!" class="btn btn-danger btn-sm">
                                    <li class="fa fa-trash"> Hapus</li>
                                </a>
                            </td>

                                 <!-- Modal Edit-->
                                        <div class="modal fade" id="edit-data-<?php echo $m['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="newEditModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="newEditModalLabel">Edit Menu</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="<?= base_url('menu/ubah'); ?>" method="post">
                                                        <div class="modal-body">
                                                            <input name="id" type="hidden" value="<?= $m['id']; ?>">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu" value="<?= $m['menu']; ?>">
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="Submit" class="btn btn-primary">Update </button>
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                <!-- Modal Delete -->
                                        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    </div>
                                                    <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
                                                    <div class="modal-footer">
                                                        <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
                                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        
                                <!-- Modal -->
                                <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="newMenuModalLabel">Add New Menu</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action="<?= base_url('menu'); ?>" method="post">
                                                <div class="modal-body">

                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Menu">
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="Submit" class="btn btn-primary">Add </button>
                                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                

                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    function deleteConfirm(url) {
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>