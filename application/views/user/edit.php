<!-- Begin Page Content -->
<div class="container-fluid">

	<div class="container">

		   <?php
            $this->load->helper('form');
            $error = $this->session->flashdata('error');
            if ($error) {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $error; ?>
                </div>
            <?php }
            $success = $this->session->flashdata('success');
            if ($success) {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $success; ?>
                </div>
            <?php } ?>

		 <form role="form" data-parsley-validate="" action="<?php echo base_url() ?>user/edit_process" method="post" enctype="multipart/form-data">
			  <div class="form-group">

			  	 <input type="hidden" name="id" id="id" value="<?= $user['id'] ?>">
			    <label for="exampleInputEmail1">Username</label>
			    <input type="text" id="username" val name="username" class="form-control col-md-7 col-xs-12" value="<?= $user['username']; ?>" readonly>
			  </div>
			  <div class="form-group">
			    <label for="exampleInputPassword1">Password</label>
			     <input type="text" id="name" name="name" class="form-control col-md-7 col-xs-12" value="<?= $user['name']; ?>">
			  </div>
			  <div class="form-group">
				    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image" id="image">
				    <br>
				   <img src="<?php echo base_url(); ?>assets/img/profile/<?= $user['image'] ?>" onerror="this.src='<?= base_url('assets/img/profile/default.png'); ?>';" alt="Missing Image" width="100px" />
			  </div>
			  <hr>
			    <h5 style="color: red" class="">Kosongi password jika tidak ingin diganti !!</h5>
			    <div class="form-group">
			        <label class="control-label" for="password">Password <span>*</span>
			        </label>
			            <input type="text" id="password" name="password" class="form-control col-md-7 col-xs-12">

			    </div>
			    <div class="form-group">
			        <label class="control-label" for="confirm_password">Repeat Password <span>*</span>
			        </label>
			            <input type="text" id="confirm_password" name="confirm_password" class="form-control col-md-7 col-xs-12">
			        <span id='message'></span>
			    </div>
			    <div class="ln_solid"></div>
			    <div class="form-group">
			        <div class="">
			            <button type="submit" class="btn btn-primary btn-md ">
			                <li class="fa fa-paper-plane"> Save</li>
			            </button>
			        </div>
			    </div>
		</form>
	</div>
</div>
<!-- End of Main Content -->

<script type="text/javascript">
    $('#password, #confirm_password').on('keyup', function() {
        if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
            document.getElementById("submit").disabled = false;
        } else {
            $('#message').html('Not Matching').css('color', 'red');
            document.getElementById("submit").disabled = true;
        }
    });
</script>