<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-6">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
            <?= $this->session->flashdata('message'); ?>
            <form action="<?= base_url('master/edit_rule_process'); ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="rule_id" value="<?=$rule_id?>">
                 <div class="form-group">
                    <select name="id_penyakit" id="id_penyakit" class="form-control">
                        <option value="">Select Penyakit</option>
                        <?php foreach ($penyakit as $p) : ?>
                            <option value="<?= $p->id_penyakit; ?>" <?php echo(($p->id_penyakit == $id_penyakit) ? 'selected' : '');?>><?= $p->nama_penyakit; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <table class="table table-bordered" style="width: 400px;"> 
                        <tr>
                            <th>Gejala</th>
                            <th style="text-align: center;">Status</th>
                        </tr>
                        <?php foreach ($gejala as $g) : ?>
                            <tr>
                            <td><?=$g['nama_gejala']?></td>
                            <td align="center">
                                <input type="hidden" name="gejala[<?=$g['id_gejala']?>]" value="0">
                                <input type="checkbox" class="form-check-input" name="gejala[<?=$g['id_gejala']?>]" value="1" <?php echo(($g['value'] == 1) ? 'checked' : '');?>>
                            </td>
                            </tr>
                        <?php endforeach; ?> 
                    </table>
                </div>
                <button type="Submit" class="btn btn-primary">Save </button>
            </form>       
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.js'?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>


