<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-6">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#ModalaAdd"><span class="fa fa-plus"></span>Add New Menu</a>
            <table class="table table-hover" id="mydata">
                <thead>
                    <tr>
                        <th scope="col">ID Gejala</th>
                        <th scope="col">Nama Gejala</th>
                    </tr>
                </thead>
                <tbody id="show_data">


                </tbody>
            </table>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

     <!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myModalLabel">Add Gejala</h5>
                </div>
                    <form class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-xs-3" >ID Gejala</label>
                                <div class="col-xs-9">
                                    <input name="id_gejala" id="id_gejala" class="form-control" type="text" placeholder="Id Gejala" style="width:335px;" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Nama Gejala</label>
                                <div class="col-xs-9">
                                    <input name="nama_gejala" id="nama_gejala" class="form-control" type="text" placeholder="Nama Gejala" style="width:335px;" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" id="btn_simpan">Add </button>
                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL ADD-->

        <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalaEdit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Edit Gejala</h3>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Id Gejala</label>
                        <div class="col-xs-9">
                            <input name="id_gejala_edit" id="id_gejala2" class="form-control" type="text" placeholder="Id Gejala" style="width:335px;" readonly>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Gejala</label>
                        <div class="col-xs-9">
                            <input name="nama_gejala_edit" id="nama_gejala2" class="form-control" type="text" placeholder="Nama Gejala" style="width:335px;" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" id="btn_update">Update</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL EDIT-->
 
        <!--MODAL HAPUS-->
        <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Gejala</h4>
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">
                                           
                            <input type="hidden" name="kode" id="textkode" value="">
                            <div class="alert alert-warning"><p>Apakah Anda yakin mau menghapus penyakit ini?</p></div>
                                         
                    </div>
                    <div class="modal-footer">
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL HAPUS-->

<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.js'?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        tampil_data_gejala();   //pemanggilan fungsi tampil gejala.

          
        //fungsi tampil gejala
        function tampil_data_gejala(){
            $.ajax({
                type  : 'GET',
                url   : '<?php echo base_url()?>index.php/master/data_gejala',
                async : true,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<tr>'+
                                '<td>'+data[i].id_gejala+'</td>'+
                                '<td>'+data[i].nama_gejala+'</td>'+
                                '<td style="text-align:right;">'+
                                    '<a href="javascript:;" class="btn btn-info btn-xs item_edit" data="'+data[i].id_gejala+'">Edit</a>'+' '+
                                    '<a href="javascript:;" class="btn btn-danger btn-xs item_hapus" data="'+data[i].id_gejala+'">Hapus</a>'+
                                '</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }
 
            });
        }
 
        //GET UPDATE
        $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/master/get_gejala')?>",
                dataType : "JSON",
                data : {id_gejala:id},
                success: function(data){
                    $.each(data,function(id_gejala, nama_gejala){
                        $('#ModalaEdit').modal('show');
                        $('[name="id_gejala_edit"]').val(data.id_gejala);
                        $('[name="nama_gejala_edit"]').val(data.nama_gejala);
                    });
                }
            });
            return false;
        });
 
 
        //GET HAPUS
        $('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#ModalHapus').modal('show');
            $('[name="kode"]').val(id);
        });
 
        //Simpan Gejala
        $('#btn_simpan').on('click',function(){
            var id_gejala=$('#id_gejala').val();
            var nama_gejala=$('#nama_gejala').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('index.php/master/save_gejala')?>",
                dataType : "JSON",
                data : {id_gejala:id_gejala , nama_gejala:nama_gejala},
                success: function(data){
                    $('[name="id_gejala"]').val("");
                    $('[name="nama_gejala"]').val("");
                    $('#ModalaAdd').modal('hide');
                    $('.modal-backdrop').remove();
                    tampil_data_gejala();
                },error: function(data) {
                    successmessage = 'ID Sudah Gunakan';
                   alert(successmessage);
                },
            });
            return false;
        });


 
        //Update Gejala
        $('#btn_update').on('click',function(){
            var id_gejala=$('#id_gejala2').val();
            var nama_gejala=$('#nama_gejala2').val();
            var solusi=$('#solusi2').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('index.php/master/update_gejala')?>",
                dataType : "JSON",
                data : {id_gejala:id_gejala , nama_gejala:nama_gejala},
                success: function(data){
                    $('[name="id_gejala_edit"]').val("");
                    $('[name="nama_gejala_edit"]').val("");
                    $('#ModalaEdit').modal('hide');
                     $('.modal-backdrop').remove();
                    tampil_data_gejala();
                }
            });
            return false;
        });
 
        //Hapus Gejala
        $('#btn_hapus').on('click',function(){
            var kode=$('#textkode').val();
            $.ajax({
            type : "POST",
            url  : "<?php echo base_url('index.php/master/delete_gejala')?>",
            dataType : "JSON",
                    data : {id_gejala: kode},
                    success: function(data){
                            $('#ModalHapus').modal('hide');
                            tampil_data_gejala();
                    }
                });
                return false;
            });
    });
 
</script>


