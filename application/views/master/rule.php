<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-6">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="<?= base_url('master/add_rule/'); ?>" class="btn btn-primary mb-3"><span class="fa fa-plus"></span>  Add New Rule</a>
            <table class="table table-hover" id="mydata">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <!-- <th scope="col">Rule Name</th> -->
                        <th scope="col">Penyakit</th>
                        <th scope="col">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($rule as $val) : ?>
                        <tr>
                            <td><?=$i?></td>
                            <!-- <td><?=$val['rule_name']?></td> -->
                            <td><?=$val['nama_penyakit']?></td>
                            <td>
                                <a class="btn btn-warning btn-sm" href="<?= base_url('master/edit_rule/'.$val['id']); ?>">
                                    <li class="fa fa-edit"> Edit</li>
                                </a>
                                <a class="btn btn-danger btn-sm" href="<?= base_url('master/delete_rule/'.$val['id']); ?>">
                                    <li class="fa fa-trash"> Hapus</li>
                                </a>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.js'?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>


