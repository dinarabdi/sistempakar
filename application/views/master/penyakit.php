<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>


    <div class="row">
        <div class="col-lg-6">

            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#ModalaAdd"><span class="fa fa-plus"></span>Add New Menu</a>
            <table class="table table-hover" id="mydata">
                <thead>
                    <tr>
                        <th scope="col">ID Penyakit</th>
                        <th scope="col">Nama Penyakit</th>
                        <th scope="col">Solusi</th>
                    </tr>
                </thead>
                <tbody id="show_data">


                </tbody>
            </table>
        </div>
    </div>

    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

     <!-- MODAL ADD -->
        <div class="modal fade" id="ModalaAdd" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="myModalLabel">Add Penyakit</h5>
                </div>
                    <form class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="control-label col-xs-3" >ID Penyakit</label>
                                <div class="col-xs-9">
                                    <input name="id_penyakit" id="id_penyakit" class="form-control" type="text" placeholder="Id Penyakit" style="width:335px;" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Nama Penyakit</label>
                                <div class="col-xs-9">
                                    <input name="nama_penyakit" id="nama_penyakit" class="form-control" type="text" placeholder="Nama Penyakit" style="width:335px;" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-3" >Solusi</label>
                                <div class="col-xs-9">
                                    <input name="solusi" id="solusi" class="form-control" type="text" placeholder="Solusi" style="width:335px;" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" id="btn_simpan">Add </button>
                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL ADD-->

        <!-- MODAL EDIT -->
        <div class="modal fade" id="ModalaEdit" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Edit Penyakit</h3>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Id Penyakit</label>
                        <div class="col-xs-9">
                            <input name="id_penyakit_edit" id="id_penyakit2" class="form-control" type="text" placeholder="Id Penyakit" style="width:335px;" readonly>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama Penyakit</label>
                        <div class="col-xs-9">
                            <input name="nama_penyakit_edit" id="nama_penyakit2" class="form-control" type="text" placeholder="Nama Penyakit" style="width:335px;" required>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Solusi</label>
                        <div class="col-xs-9">
                            <input name="solusi_edit" id="solusi2" class="form-control" type="text" placeholder="Solusi" style="width:335px;" required>
                        </div>
                    </div>
 
                </div>
 
                <div class="modal-footer">
                    <button class="btn btn-info" id="btn_update">Update</button>
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        <!--END MODAL EDIT-->
 
        <!--MODAL HAPUS-->
        <div class="modal fade" id="ModalHapus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button>
                        <h4 class="modal-title" id="myModalLabel">Hapus Penyakit</h4>
                    </div>
                    <form class="form-horizontal">
                    <div class="modal-body">
                                           
                            <input type="hidden" name="kode" id="textkode" value="">
                            <div class="alert alert-warning"><p>Apakah Anda yakin mau menghapus penyakit ini?</p></div>
                                         
                    </div>
                    <div class="modal-footer">
                        <button class="btn_hapus btn btn-danger" id="btn_hapus">Hapus</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!--END MODAL HAPUS-->

<script type="text/javascript" src="<?php echo base_url().'assets/vendor/jquery/jquery.js'?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/vendor/datatables/jquery.dataTables.js'); ?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        tampil_data_penyakit();   //pemanggilan fungsi tampil barang.

          
        //fungsi tampil barang
        function tampil_data_penyakit(){
            $.ajax({
                type  : 'GET',
                url   : '<?php echo base_url()?>index.php/master/data_penyakit',
                async : true,
                dataType : 'json',
                success : function(data){
                    var html = '';
                    var i;
                    for(i=0; i<data.length; i++){
                        html += '<tr>'+
                                '<td>'+data[i].id_penyakit+'</td>'+
                                '<td>'+data[i].nama_penyakit+'</td>'+
                                '<td>'+data[i].solusi+'</td>'+
                                '<td style="text-align:right;">'+
                                    '<a href="javascript:;" class="btn btn-info btn-xs item_edit" data="'+data[i].id_penyakit+'">Edit</a>'+' '+
                                    '<a href="javascript:;" class="btn btn-danger btn-xs item_hapus" data="'+data[i].id_penyakit+'">Hapus</a>'+
                                '</td>'+
                                '</tr>';
                    }
                    $('#show_data').html(html);
                }
 
            });
        }
 
        //GET UPDATE
        $('#show_data').on('click','.item_edit',function(){
            var id=$(this).attr('data');
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('index.php/master/get_penyakit')?>",
                dataType : "JSON",
                data : {id_penyakit:id},
                success: function(data){
                    $.each(data,function(id_penyakit, nama_penyakit, solusi){
                        $('#ModalaEdit').modal('show');
                        $('[name="id_penyakit_edit"]').val(data.id_penyakit);
                        $('[name="nama_penyakit_edit"]').val(data.nama_penyakit);
                        $('[name="solusi_edit"]').val(data.solusi);
                    });
                }
            });
            return false;
        });
 
 
        //GET HAPUS
        $('#show_data').on('click','.item_hapus',function(){
            var id=$(this).attr('data');
            $('#ModalHapus').modal('show');
            $('[name="kode"]').val(id);
        });
 
        //Simpan Penyakit
        $('#btn_simpan').on('click',function(){
            var id_penyakit=$('#id_penyakit').val();
            var nama_penyakit=$('#nama_penyakit').val();
            var solusi=$('#solusi').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('index.php/master/save_penyakit')?>",
                dataType : "JSON",
                data : {id_penyakit:id_penyakit , nama_penyakit:nama_penyakit, solusi:solusi},
                success: function(data){
                    $('[name="id_penyakit"]').val("");
                    $('[name="nama_penyakit"]').val("");
                    $('[name="solusi"]').val("");
                    $('#ModalaAdd').modal('hide');
                    $('.modal-backdrop').remove();
                    tampil_data_penyakit();
                },error: function(data) {
                    successmessage = 'ID Sudah Gunakan';
                   alert(successmessage);
                },
            });
            return false;
        });


 
        //Update Penyakit
        $('#btn_update').on('click',function(){
            var id_penyakit=$('#id_penyakit2').val();
            var nama_penyakit=$('#nama_penyakit2').val();
            var solusi=$('#solusi2').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('index.php/master/update_penyakit')?>",
                dataType : "JSON",
                data : {id_penyakit:id_penyakit , nama_penyakit:nama_penyakit, solusi:solusi},
                success: function(data){
                    $('[name="id_penyakit_edit"]').val("");
                    $('[name="nama_penyakit_edit"]').val("");
                    $('[name="solusi_edit"]').val("");
                    $('#ModalaEdit').modal('hide');
                     $('.modal-backdrop').remove();
                    tampil_data_penyakit();
                }
            });
            return false;
        });
 
        //Hapus Penyakit
        $('#btn_hapus').on('click',function(){
            var kode=$('#textkode').val();
            $.ajax({
            type : "POST",
            url  : "<?php echo base_url('index.php/master/delete_penyakit')?>",
            dataType : "JSON",
                    data : {id_penyakit: kode},
                    success: function(data){
                            $('#ModalHapus').modal('hide');
                            tampil_data_penyakit();
                    }
                });
                return false;
            });
 
    });
 
</script>


