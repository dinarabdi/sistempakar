-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Mar 2020 pada 11.06
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sp_foward`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `article`
--

CREATE TABLE `article` (
  `id_article` int(11) NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `file` varchar(128) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `article`
--

INSERT INTO `article` (`id_article`, `title`, `file`, `content`, `created_at`, `update_at`, `update_by`) VALUES
(12, 'Test Dinar', '2.jpg', '<p>1</p>\r\n', '2020-03-14 13:08:03', '2020-03-15 19:29:22', 2),
(13, 'Manage Students', '35.jpg', '<p>hhhh</p>\r\n', '2020-03-15 00:21:07', '2020-03-15 00:21:07', 1),
(14, 'dede1', '1.jpg', '<p>qwqw</p>\r\n', '2020-03-15 00:21:15', '2020-03-15 00:21:15', 1),
(15, 'dede3', '1.jpg', '<p>qwqw</p>\r\n', '2020-03-15 00:21:15', '2020-03-15 00:21:15', 1),
(16, 'dede2', '1.jpg', '<p>qwqw</p>\r\n', '2020-03-15 00:21:15', '2020-03-15 00:21:15', 1),
(17, 'dede4', '1.jpg', '<p>qwqw</p>\r\n', '2020-03-15 00:21:15', '2020-03-15 00:21:15', 1),
(18, 'coba', '36.jpg', '<p>1122</p>\r\n', '2020-03-15 21:50:04', '2020-03-15 21:50:04', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `barang_kode` varchar(30) NOT NULL,
  `barang_nama` varchar(30) NOT NULL,
  `barang_harga` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `beasiswa`
--

CREATE TABLE `beasiswa` (
  `id_beasiswa` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `keterangan` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `updated_at` datetime NOT NULL,
  `created_by` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `beasiswa`
--

INSERT INTO `beasiswa` (`id_beasiswa`, `name`, `keterangan`, `gambar`, `created_at`, `updated_at`, `created_by`) VALUES
(2, 'Beasiswa Berprestasi', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '3.jpg', '2020-03-16 12:25:18', '2020-03-16 12:25:18', '2'),
(6, 'Beasiswa Kalasan', '<p>1233</p>\r\n', '3.jpg', '2020-03-16 11:45:31', '2020-03-15 20:39:33', '2'),
(7, 'dinar', '<p>asdas</p>\r\n', '3.jpg', '2020-03-16 11:45:32', '2020-03-16 10:54:15', '1'),
(8, 'test', '<p>123</p>\r\n', '3.jpg', '2020-03-16 10:55:24', '2020-03-16 10:55:24', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `id_gejala` varchar(30) NOT NULL,
  `nama_gejala` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`id_gejala`, `nama_gejala`) VALUES
('1', '12'),
('2', '123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_akhir`
--

CREATE TABLE `hasil_akhir` (
  `id_hasil_akhir` int(11) NOT NULL,
  `id_peserta_penilaian` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `hasil_akhir`
--

INSERT INTO `hasil_akhir` (`id_hasil_akhir`, `id_peserta_penilaian`, `id_kriteria`, `value`) VALUES
(1, 47, 3, 0.063960214906683),
(2, 47, 5, 0.24494897427832),
(3, 47, 6, 0.093704257133164),
(4, 47, 19, 0.097014250014533),
(5, 47, 20, 0.060302268915553),
(6, 48, 3, 0.063960214906683),
(7, 48, 5, 0.12247448713916),
(8, 48, 6, 0.12493900951088),
(9, 48, 19, 0.097014250014533),
(10, 48, 20, 0.18090680674666),
(11, 49, 3, 0.042640143271122),
(12, 49, 5, 0.12247448713916),
(13, 49, 6, 0.12493900951088),
(14, 49, 19, 0.1455213750218),
(15, 49, 20, 0.060302268915553),
(16, 50, 3, 0.040824829046386),
(17, 50, 5, 0.23533936216582),
(18, 50, 6, 0.045883146774112),
(19, 50, 19, 0.13764944032234),
(20, 50, 20, 0.12792042981337),
(21, 51, 3, 0.040824829046386),
(22, 51, 5, 0.17650452162437),
(23, 51, 6, 0.13764944032234),
(24, 51, 19, 0.13764944032234),
(25, 51, 20, 0.085280286542244),
(26, 52, 3, 0.081649658092773),
(27, 52, 5, 0.058834840541455),
(28, 52, 6, 0.13764944032234),
(29, 52, 19, 0.045883146774112),
(30, 52, 20, 0.12792042981337),
(31, 53, 15, 0.23094010767585),
(32, 53, 16, 0.22386075217339),
(33, 53, 17, 0.0727606875109),
(34, 53, 18, 0.055708601453116),
(35, 54, 15, 0.23094010767585),
(36, 54, 16, 0.22386075217339),
(37, 54, 17, 0.0727606875109),
(38, 54, 18, 0.03713906763541),
(39, 55, 15, 0.23094010767585),
(40, 55, 16, 0.14924050144893),
(41, 55, 17, 0.10914103126635),
(42, 55, 18, 0.074278135270821),
(43, 56, 15, 0.29711254108328),
(44, 56, 16, 0.2857738033247),
(45, 56, 17, 0.1309307341416),
(46, 56, 18, 0.063960214906683),
(47, 57, 15, 0.14855627054164),
(48, 57, 16, 0.14288690166235),
(49, 57, 17, 0.032732683535399),
(50, 57, 18, 0.063960214906683),
(51, 58, 15, 0.22283440581246),
(52, 58, 16, 0.14288690166235),
(53, 58, 17, 0.065465367070798),
(54, 58, 18, 0.042640143271122);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(11) NOT NULL,
  `beasiswa_id` int(11) DEFAULT NULL,
  `name_kriteria` varchar(50) DEFAULT NULL,
  `type_kriteria` enum('0','1') DEFAULT NULL,
  `weight_kriteria` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `beasiswa_id`, `name_kriteria`, `type_kriteria`, `weight_kriteria`, `created_at`, `updated_at`, `created_by`) VALUES
(3, 2, 'Nilai Rapot', '1', 10, '2019-11-23 12:38:39', '2019-11-26 09:11:31', '1'),
(5, 2, 'Tanggungan Orang Tua', '1', 30, '2019-11-25 01:28:05', '2019-11-26 09:11:31', '1'),
(6, 2, 'Pendapatan Orang Tua', '0', 20, '2019-11-23 12:39:17', '2019-11-26 09:11:31', '1'),
(15, 6, 'Penghasilan Orang Tua', '0', 40, '2019-11-23 14:52:28', '2019-11-23 14:52:28', '1'),
(16, 6, 'nilai rapot', '1', 35, '2019-11-23 14:53:10', '2019-11-23 14:53:10', '1'),
(17, 6, 'Jumlah tanggungan orang tua', '1', 15, '2019-11-23 14:53:32', '2019-11-23 14:53:32', '1'),
(18, 6, 'Presentase kehadiran siswa', '1', 10, '2019-11-23 14:53:50', '2019-11-23 14:53:50', '1'),
(19, 2, 'Presentasi Kehadiran Siswa', '1', 20, '2019-11-26 06:36:24', '2019-11-26 09:11:31', '1'),
(20, 2, 'Jumlah Saudra Kandung', '1', 20, '2019-11-26 06:38:38', '2019-11-26 09:11:31', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `normalisasi`
--

CREATE TABLE `normalisasi` (
  `id_normalisasi` int(11) NOT NULL,
  `id_peserta_penilaian` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `normalisasi`
--

INSERT INTO `normalisasi` (`id_normalisasi`, `id_peserta_penilaian`, `id_kriteria`, `value`) VALUES
(128, 47, 3, 0.63960214906683),
(129, 47, 5, 0.81649658092773),
(130, 47, 6, 0.46852128566582),
(131, 47, 19, 0.48507125007267),
(132, 47, 20, 0.30151134457776),
(133, 48, 3, 0.63960214906683),
(134, 48, 5, 0.40824829046386),
(135, 48, 6, 0.62469504755442),
(136, 48, 19, 0.48507125007267),
(137, 48, 20, 0.90453403373329),
(138, 49, 3, 0.42640143271122),
(139, 49, 5, 0.40824829046386),
(140, 49, 6, 0.62469504755442),
(141, 49, 19, 0.727606875109),
(142, 49, 20, 0.30151134457776),
(143, 50, 3, 0.40824829046386),
(144, 50, 5, 0.78446454055274),
(145, 50, 6, 0.22941573387056),
(146, 50, 19, 0.68824720161169),
(147, 50, 20, 0.63960214906683),
(148, 51, 3, 0.40824829046386),
(149, 51, 5, 0.58834840541455),
(150, 51, 6, 0.68824720161169),
(151, 51, 19, 0.68824720161169),
(152, 51, 20, 0.42640143271122),
(153, 52, 3, 0.81649658092773),
(154, 52, 5, 0.19611613513818),
(155, 52, 6, 0.68824720161169),
(156, 52, 19, 0.22941573387056),
(157, 52, 20, 0.63960214906683),
(158, 53, 15, 0.57735026918963),
(159, 53, 16, 0.63960214906683),
(160, 53, 17, 0.48507125007267),
(161, 53, 18, 0.55708601453116),
(162, 54, 15, 0.57735026918963),
(163, 54, 16, 0.63960214906683),
(164, 54, 17, 0.48507125007267),
(165, 54, 18, 0.3713906763541),
(166, 55, 15, 0.57735026918963),
(167, 55, 16, 0.42640143271122),
(168, 55, 17, 0.727606875109),
(169, 55, 18, 0.74278135270821),
(170, 56, 15, 0.74278135270821),
(171, 56, 16, 0.81649658092773),
(172, 56, 17, 0.87287156094397),
(173, 56, 18, 0.63960214906683),
(174, 57, 15, 0.3713906763541),
(175, 57, 16, 0.40824829046386),
(176, 57, 17, 0.21821789023599),
(177, 57, 18, 0.63960214906683),
(178, 58, 15, 0.55708601453116),
(179, 58, 16, 0.40824829046386),
(180, 58, 17, 0.43643578047198),
(181, 58, 18, 0.42640143271122);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penilaian`
--

CREATE TABLE `penilaian` (
  `id_penilaian` int(11) NOT NULL,
  `id_beasiswa` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `penilaian`
--

INSERT INTO `penilaian` (`id_penilaian`, `id_beasiswa`, `created_at`, `created_by`) VALUES
(1, 2, '2020-01-21 11:53:45', 1),
(2, 2, '2020-01-21 11:54:21', 1),
(3, 6, '2020-03-15 12:32:41', 1),
(4, 6, '2020-03-16 12:39:56', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyakit`
--

CREATE TABLE `penyakit` (
  `id_penyakit` varchar(30) NOT NULL,
  `nama_penyakit` varchar(100) NOT NULL,
  `solusi` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penyakit`
--

INSERT INTO `penyakit` (`id_penyakit`, `nama_penyakit`, `solusi`) VALUES
('1', 'FLu Kucing', 'FLu Kucing');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peserta_penilaian`
--

CREATE TABLE `peserta_penilaian` (
  `id_peserta_penilaian` int(11) NOT NULL,
  `id_penilaian` int(11) NOT NULL,
  `id_students` int(11) NOT NULL,
  `total` double NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `peserta_penilaian`
--

INSERT INTO `peserta_penilaian` (`id_peserta_penilaian`, `id_penilaian`, `id_students`, `total`, `created_at`) VALUES
(47, 1, 4, 0.37252145098192, '2020-01-21 11:53:45'),
(48, 1, 3, 0.33941674929615, '2020-01-21 11:53:45'),
(49, 1, 2, 0.24599926483675, '2020-01-21 11:53:45'),
(50, 2, 8, 0.4958509145738, '2020-01-21 11:54:21'),
(51, 2, 6, 0.302609637213, '2020-01-21 11:54:21'),
(52, 2, 9, 0.17663863489937, '2020-01-21 11:54:21'),
(53, 3, 2, 0.12138993346156, '2020-03-15 12:32:41'),
(54, 3, 8, 0.10282039964385, '2020-03-15 12:32:41'),
(55, 3, 3, 0.10171956031025, '2020-03-15 12:32:41'),
(56, 4, 2, 0.1835522112897, '2020-03-16 12:39:56'),
(57, 4, 6, 0.091023529562793, '2020-03-16 12:39:56'),
(58, 4, 5, 0.02815800619181, '2020-03-16 12:39:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `students`
--

CREATE TABLE `students` (
  `id_students` int(11) NOT NULL,
  `nis` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `place_birth` varchar(50) NOT NULL,
  `birthday` date NOT NULL,
  `gender` enum('1','0') NOT NULL,
  `religion` varchar(20) NOT NULL,
  `address` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `students`
--

INSERT INTO `students` (`id_students`, `nis`, `name`, `place_birth`, `birthday`, `gender`, `religion`, `address`, `created_at`, `update_at`) VALUES
(2, '82547', 'dinar', 'tangerangg', '2019-10-03', '1', 'Islam', 'jln kp kadu 04/01 curug tangerang', '2019-10-31 13:27:33', '2020-01-21 11:52:46'),
(3, '28613', 'rere', 'tangerang', '2019-11-16', '0', 'Islam', 'jln kp kadu 04/01 curug tangerang', '2019-11-01 01:31:13', '2019-11-26 06:59:52'),
(4, '36315', 'Budi', 'Klaten', '1993-06-16', '1', 'Islam', 'Klaten', '2019-11-23 12:55:46', '2019-11-26 07:00:11'),
(5, '21056', 'Anis', 'Klaten', '2019-11-02', '0', 'Kristen', 'Klaten', '2019-11-24 15:20:32', '2019-11-26 07:00:52'),
(6, '72605', 'Risa', 'Yogya', '2003-02-04', '0', 'Hindu', 'Yogya', '2019-11-23 14:19:13', '2019-11-26 07:01:41'),
(7, '72606', 'Doni', 'sleman', '2019-11-03', '1', 'Islam', 'Sleman', '2019-11-23 14:19:28', '2019-11-26 07:02:23'),
(8, '72607', 'Rina', 'Sleman', '2019-11-14', '0', 'Islam', 'Sleman', '2019-11-23 14:19:42', '2019-11-26 07:02:53'),
(9, '72601', 'Arif', 'Gunung Kidul', '2019-11-03', '1', 'Islam', 'Gunung Kidul', '2019-11-23 14:19:59', '2019-11-26 07:03:33'),
(10, '72602', 'Riska', 'Klaten', '2019-11-03', '0', 'Islam', 'Klaten', '2019-11-23 14:20:11', '2019-11-26 07:04:08'),
(11, '50599', 'Roni', 'Wonogiri', '2019-11-03', '1', 'Islam', 'Wonogiri', '2019-11-23 14:20:37', '2019-11-26 07:04:43'),
(13, '50591', 'Irvan', 'Sukoharjo', '2015-03-04', '1', 'Islam', 'Sukoharjo', '2019-11-24 14:53:52', '2019-11-26 07:05:30'),
(14, '50597', 'Dian', 'tangerangg', '2019-10-07', '0', 'Islam', 'jln kp kadu 04/01 curug tangerang', '2019-10-31 13:27:33', '2019-11-26 07:06:01'),
(15, '50598', 'Aris', 'Solo', '2019-11-10', '1', 'Islam', 'Solo', '2019-11-23 14:19:13', '2019-11-26 07:06:42'),
(16, '38512', 'rere', 'tangerang', '2019-11-16', '0', 'Islam', 'jln kp kadu 04/01 curug tangerang', '2019-11-01 01:31:13', '2019-11-26 07:06:57'),
(17, '38513', 'Dwi', 'Klaten', '2019-11-10', '1', 'Islam', 'Klaten', '2019-11-23 14:19:13', '2019-11-26 07:07:17'),
(18, '38511', 'Asih', 'Yogya', '2019-11-10', '1', 'Islam', 'Yogya', '2019-11-23 14:19:13', '2019-11-26 07:07:45'),
(19, '38514', 'Damar', 'Solo', '2019-10-07', '1', 'Islam', 'Solo', '2019-10-31 13:27:33', '2019-11-26 07:08:22'),
(20, '38515', 'Dani', 'Klaten', '2019-11-10', '1', 'Islam', 'Klaten', '2019-11-23 14:19:13', '2019-11-26 07:08:43'),
(21, '38518', 'Rosa', 'Gunung Kidul', '2019-10-07', '0', 'Islam', 'Gunung Kidul', '2019-10-31 13:27:33', '2019-11-26 07:09:22'),
(22, '95242', 'Arfa', 'tangerangg', '2019-10-07', '1', 'Islam', 'jln kp kadu 04/01 curug tangerang', '2019-10-31 13:27:33', '2019-11-26 07:09:40'),
(23, '95241', 'Dayat', 'Klaten', '2019-10-07', '1', 'Islam', 'Klaten', '2019-10-31 13:27:33', '2019-11-26 07:09:59'),
(24, '95244', 'Dono', 'Kulon Progo', '2019-10-07', '1', 'Islam', 'Kulon Progo', '2019-10-31 13:27:33', '2019-11-26 07:10:25'),
(25, '60602', 'Chandra', 'tangerangg', '2019-10-07', '1', 'Islam', 'jln kp kadu 04/01 curug tangerang', '2019-11-24 15:18:52', '2019-11-26 07:10:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_kriteria`
--

CREATE TABLE `sub_kriteria` (
  `id_sub_kriteria` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `keterangan` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `fuzzy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `sub_kriteria`
--

INSERT INTO `sub_kriteria` (`id_sub_kriteria`, `id_kriteria`, `keterangan`, `fuzzy`, `value`) VALUES
(78, 15, '< 1 jt', NULL, 4),
(79, 15, '1 jt - 3 jt', NULL, 3),
(80, 15, '3 jt - 5 jt', NULL, 2),
(81, 15, '> 5 jt', NULL, 1),
(82, 16, '<= 75', NULL, 1),
(83, 16, '< 75 - >=80', NULL, 2),
(84, 16, '< 80 - >=85', NULL, 3),
(85, 16, '> 85', NULL, 4),
(86, 17, '1 anak', NULL, 1),
(87, 17, '2 anak', NULL, 2),
(88, 17, '3 anak', NULL, 3),
(89, 17, '4 anak', NULL, 4),
(90, 18, '< 85%', NULL, 1),
(91, 18, '< 85% - >=90%', NULL, 2),
(92, 18, '<90 % - >= 95%', NULL, 3),
(93, 18, '> 95%', NULL, 4),
(101, 3, 'Nilai <75', 'Sangat Rendah', 1),
(102, 3, 'Nilai >75 X < 85', 'Sedang', 2),
(103, 3, 'Nilai >85 X < 95', 'Bagus', 3),
(104, 3, 'Nilai >=95', 'Sangat Bagus', 4),
(105, 5, '1 anak', 'Sangat Rendah', 1),
(106, 5, '2 anak', 'Sedang', 2),
(107, 5, '3 anak', 'Bagus', 3),
(108, 5, '>4 anak', 'Sangat Bagus', 4),
(109, 6, '< 1.000.000', 'Sangat Bagus', 4),
(110, 6, '>= 1.000.000 X <=3.000.000', 'Bagus', 3),
(111, 6, '> 3.000.000 X <=5.000.000', 'Sedang', 2),
(112, 6, '> 5.000.000', 'Sangat Rendah', 1),
(113, 19, '< 85%', 'Sangat Rendah', 1),
(115, 19, '>= 85% X <=90%', 'Sedang', 2),
(116, 19, '>= 91% X <95%', 'Bagus', 3),
(117, 19, '>=95%', 'Sangat Bagus', 4),
(118, 20, '1 Orang', 'Sangat Rendah ', 1),
(119, 20, '2 Orang', 'Sedang', 2),
(120, 20, '3 Orang', 'Bagus', 3),
(121, 20, '>4 Orang', 'Sangat Bagus', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tentang`
--

CREATE TABLE `tentang` (
  `id` int(1) NOT NULL,
  `tentang` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tentang`
--

INSERT INTO `tentang` (`id`, `tentang`, `visi`, `misi`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'admin', 'admin', 'test7.png', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '2019-10-13 00:00:00'),
(2, 'dinar', 'dinar', '17_12_00022.jpg', '13d2c27d75f43e084f96904768e10fee', 2, 1, '2019-10-15 20:49:19'),
(3, 'Dani', 'dinar123', 'IMG_7772.JPG', '7499ab1fa766f5e695b6eb74cfa3b11b', 2, 1, '2019-11-01 07:33:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(3, 2, 2),
(9, 1, 4),
(11, 1, 3),
(16, 2, 5),
(20, 1, 5),
(21, 1, 7),
(23, 2, 7),
(25, 2, 3),
(26, 1, 2),
(29, 1, 10),
(30, 2, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Staff'),
(5, 'Master'),
(7, 'Report'),
(10, 'News');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'administrator'),
(2, 'user');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fa fa-tachometer', 1),
(2, 2, 'My Profile', 'user', 'fa fa-user', 1),
(3, 2, 'Edit Profile', 'user/edit', 'fa fa-edit', 1),
(4, 3, 'Menu Management', 'menu/index', 'fa fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fa fa-folder-open', 1),
(14, 1, 'Role Access', 'admin/role', 'fa fa-key', 1),
(15, 4, 'Manage Users', 'staff', 'fa fa-users', 1),
(16, 5, 'Penyakit', 'master', 'fa fa-heartbeat', 1),
(20, 7, 'Assessment History', 'report/assessment_history', 'fa fa-bar-chart', 1),
(22, 10, 'Article', 'news', 'fa fa-newspaper-o', 1),
(26, 5, 'Gejala Penyakit', 'master/gejala', 'fa fa-medkit', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id_article`);

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`barang_kode`);

--
-- Indeks untuk tabel `beasiswa`
--
ALTER TABLE `beasiswa`
  ADD PRIMARY KEY (`id_beasiswa`);

--
-- Indeks untuk tabel `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id_gejala`);

--
-- Indeks untuk tabel `hasil_akhir`
--
ALTER TABLE `hasil_akhir`
  ADD PRIMARY KEY (`id_hasil_akhir`),
  ADD KEY `peserta_penilaian1` (`id_peserta_penilaian`),
  ADD KEY `id_kriteria3` (`id_kriteria`);

--
-- Indeks untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`),
  ADD KEY `beasiswa1` (`beasiswa_id`);

--
-- Indeks untuk tabel `normalisasi`
--
ALTER TABLE `normalisasi`
  ADD PRIMARY KEY (`id_normalisasi`),
  ADD KEY `Peserta_penilaian2` (`id_peserta_penilaian`),
  ADD KEY `id_kriteria2` (`id_kriteria`);

--
-- Indeks untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD PRIMARY KEY (`id_penilaian`),
  ADD KEY `b1` (`id_beasiswa`);

--
-- Indeks untuk tabel `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`id_penyakit`);

--
-- Indeks untuk tabel `peserta_penilaian`
--
ALTER TABLE `peserta_penilaian`
  ADD PRIMARY KEY (`id_peserta_penilaian`),
  ADD KEY `id_penilaian1` (`id_penilaian`),
  ADD KEY `id_students1` (`id_students`);

--
-- Indeks untuk tabel `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_students`);

--
-- Indeks untuk tabel `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD PRIMARY KEY (`id_sub_kriteria`),
  ADD KEY `id_kriteria1` (`id_kriteria`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Role1` (`role_id`),
  ADD KEY `Menu1` (`menu_id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `submenu` (`menu_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `article`
--
ALTER TABLE `article`
  MODIFY `id_article` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `beasiswa`
--
ALTER TABLE `beasiswa`
  MODIFY `id_beasiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `hasil_akhir`
--
ALTER TABLE `hasil_akhir`
  MODIFY `id_hasil_akhir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `normalisasi`
--
ALTER TABLE `normalisasi`
  MODIFY `id_normalisasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  MODIFY `id_penilaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `peserta_penilaian`
--
ALTER TABLE `peserta_penilaian`
  MODIFY `id_peserta_penilaian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT untuk tabel `students`
--
ALTER TABLE `students`
  MODIFY `id_students` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  MODIFY `id_sub_kriteria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `hasil_akhir`
--
ALTER TABLE `hasil_akhir`
  ADD CONSTRAINT `id_kriteria3` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id_kriteria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `peserta_penilaian1` FOREIGN KEY (`id_peserta_penilaian`) REFERENCES `peserta_penilaian` (`id_peserta_penilaian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kriteria`
--
ALTER TABLE `kriteria`
  ADD CONSTRAINT `beasiswa1` FOREIGN KEY (`beasiswa_id`) REFERENCES `beasiswa` (`id_beasiswa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `normalisasi`
--
ALTER TABLE `normalisasi`
  ADD CONSTRAINT `Peserta_penilaian2` FOREIGN KEY (`id_peserta_penilaian`) REFERENCES `peserta_penilaian` (`id_peserta_penilaian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_kriteria2` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id_kriteria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `penilaian`
--
ALTER TABLE `penilaian`
  ADD CONSTRAINT `b1` FOREIGN KEY (`id_beasiswa`) REFERENCES `beasiswa` (`id_beasiswa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `peserta_penilaian`
--
ALTER TABLE `peserta_penilaian`
  ADD CONSTRAINT `id_penilaian1` FOREIGN KEY (`id_penilaian`) REFERENCES `penilaian` (`id_penilaian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_students1` FOREIGN KEY (`id_students`) REFERENCES `students` (`id_students`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `sub_kriteria`
--
ALTER TABLE `sub_kriteria`
  ADD CONSTRAINT `id_kriteria1` FOREIGN KEY (`id_kriteria`) REFERENCES `kriteria` (`id_kriteria`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD CONSTRAINT `Menu1` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Role1` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD CONSTRAINT `submenu` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
